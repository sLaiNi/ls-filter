package a10;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		in.close();
		int laenge = input.length();
		int weg = 0;
		int konvertiert = 0;
		
		//Entferne Anfangsleerzeichen
		Pattern pat = Pattern.compile("^\\s");
		Matcher mat = pat.matcher(input);
		while(mat.find())
		{
			input = input.replaceFirst("^\\s", "");
			weg++;
			mat = pat.matcher(input); //setze den Matcher auf den neuen String
		}
		System.out.println("Entfernte Leerzeichen am Anfang, neuer String: " + input + " Entfernte Zeichen: " + weg);
		
		if(Character.isUpperCase(input.charAt(0)) == false)
		{
			input = Character.toUpperCase(input.charAt(0)) + input.substring(1);
			konvertiert++;
			System.out.println("Ersetzte Satzanfang, Neuer String: " + input);
		}
		//Entferne multiple Leerzeichen zu einem
		pat = Pattern.compile("\\s(\\s)");
		mat = pat.matcher(input);
		while(mat.find())
		{
			input = input.replaceFirst("\\s(\\s)", " "); //Immer wenn zwei Spaces gefunden -> Entferne ein Space
			weg++;
			mat = pat.matcher(input); //setze den Matcher auf den neuen String
		}
		System.out.println("neue Weg: " + weg + " String: " + input);
		
		pat = Pattern.compile("^(\\s+)?(\\w)(.*)");
		mat = pat.matcher(input);
		String[] teilinput;
		teilinput = input.split("[.?,;!]");
		for(String teilsatz : teilinput)
		{
			//^(\s+)?(\w)(.*) -> mit diesem regex finde leerzeichen vorne, ersetze buchstaben am anfange, danach h�nge rest an
		}
	}

}
