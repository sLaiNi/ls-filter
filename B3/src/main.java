import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class main {

	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(System.in);
		String dateiname = in.next();
		in.close();
		System.out.println(dateiname);
		File f = new File(dateiname);
		if(f.exists())
			System.out.println("Datei existiert");
		else
			System.out.println("Datei existiert nicht");
		
		FileInputStream fin;
		fin = new FileInputStream(dateiname);
		int curEl;
		while((curEl = fin.read()) != -1) {
			System.out.print(encodeChar(curEl));
		}
		fin.close();
	}
	public static int decodeChar (char c) {
		return ((int) c);
	}
	
	public static char encodeChar (int i) {
		return ((char) i);
	}
}